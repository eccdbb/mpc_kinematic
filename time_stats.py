import time
import numpy as np

sleep_time = 0.1
z = []
for i in range(100):
    t0 = time.time_ns()
    time.sleep(sleep_time)
    t1 = time.time_ns()
    print((t1-t0)/1e9)
    z.append((t1-t0)/1e9)
print('mean:', np.mean(z))
print('std:', np.std(z))

# Lenovo X1 Yoga 3rd gen, Windows 10
# mean: 0.10948365900000001
# std: 0.0015269672616068095

# google colab
# mean: 0.10024183495000001
# std: 0.00015654000368368358