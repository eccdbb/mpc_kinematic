# MPC (Kinematic) #
# Setup #
    conda install -c conda-forge cyipopt
    conda install scipy
    conda install numpy
# Description #
(Re-)implementation of MPC from Masters thesis using Python, IPOPT, and JAX.  Uses kinematic model (for now).

De-couple components and run containers for: redis server, reference generator, controller, plant, visualization, logging 

![block diagram](./block_diagram.drawio.png "block diagram")


# TODO #
- docker
- learned problem variables initialization

### current state ###
[clock] --> [signal generator]
```
# launch a EC2 Ubuntu, 2GB ram or more
# install git, docker
# git clone/pull this
cd mpc_kinematic
sudo docker build -f dockerfile-signal_gen2 .
sudo docker-compose up
```
At this point, clock.py should be publishing ticks to the redis container.

TODO: include signal_generators.py into docker-compose

### running redis in Windows WSL ###
https://developer.redis.com/create/windows/
```
sudo apt-add-repository ppa:redislabs/redis
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install redis-server

sudo service redis-server restart

$ redis-cli 
127.0.0.1:6379> set user:1 "Jane"
127.0.0.1:6379> get user:1
"Jane"
```

### Running redis in Ubuntu ###
```
sudo systemctl restart redis
```

### Links
https://renehernandez.io/snippets/cleaning-local-docker-cache/

https://leimao.github.io/blog/Docker-Locale/

https://pythonspeed.com/articles/activate-conda-dockerfile/

https://www.educative.io/blog/docker-compose-tutorial