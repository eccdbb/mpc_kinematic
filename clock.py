import threading
import time
from datetime import datetime
import redis
import logging
import os

class ClockGen():
    def __init__(self, name:str, Td:float):
        '''
        name: name of clock
        Td: clock period (time between ticks)
        
        example:
        name = clkA
        redis channel -> clock_clkA
        '''
        self.Td = Td
        self.channel_name = 'clock_' + name
        time.sleep(1)
        try:
            host = os.environ['REDIS_URL']
            port = os.environ['PORT']
            self.r = redis.Redis(host=host, port = int(port))
            self.p = self.r.pubsub(ignore_subscribe_messages=True)
            self.p.subscribe(self.channel_name)
        except:
            print('failed to initialize redis')
        
        self.main() #
        return
    
    def main(self):
        while True:
            try:
                self.r.publish(self.channel_name,'tick')
                time.sleep(self.Td)
            except:
                print('could not publish to redis server')
# --------------------------

# class ClockListener:
#     def __init__(self, name:str, Td:float):
#         self.ch_name = name
#         self.Td = Td
#
#         channel_name = 'clock_' + name
#
#         self.r = redis.Redis()
#         self.p = self.r.pubsub(ignore_subscribe_messages=True)
#         self.p.subscribe(channel_name)
#
#         use_method = 3
#
#         thread = threading.Thread(target=self.listen_to_clock, args=(use_method))
#         thread.start()
#         return
#
#     def listen_to_clock(self, use_method):
#         if use_method == 1: # poll for new messages
#             while True:
#                 msg =



def clock_listener(use_method=3):
    ch_name = 'clock_clkA'
    Td = 1.0

    host = os.environ['REDIS_URL']
    port = os.environ['PORT']
    r = redis.Redis(host = host, port = int(port))
    p = r.pubsub(ignore_subscribe_messages=True)
    p.subscribe(ch_name)
    
    # use_method = 3
    
    if use_method == 1:  # poll for new messages; this will block the thread
        while True:
            msg = p.get_message(ignore_subscribe_messages=True)
            if msg:
                # do something
                if msg['type'] == 'message':
                    data = msg['data']
                    print(data.decode())
            time.sleep(.001)
    elif use_method == 2: # this will block the thread
        for msg in p.listen():
            if msg['type'] == 'message':
                data = msg['data']
                print(data.decode())
    elif use_method == 3: # listen in another thread
        def my_handler(msg):
            if msg['type'] == 'message':
                data = msg['data']
                print(data.decode() + ' at ' + datetime.now().strftime('%Y.%m.%d-%H:%M:%S.%f'))
                
            # do other stuff; ex. run a task (MPC, update 3D pose, ...)
            return
        
        p.subscribe(**{ch_name: my_handler})
        thread = p.run_in_thread()
    
        # thread.stop()
    return

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    
    # first listen in another thread (for debugging)
    logging.info('starting clock listener')
    clock_listener(use_method=3)
    
    # then run clock
    logging.info('starting clock generator')
    clk = ClockGen(Td = 1.0, name='clkA')