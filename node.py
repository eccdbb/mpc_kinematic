import redis
import logging
import threading
from datetime import datetime
import time

class Node:
    '''
    node in the  system architecture graph.
    each node contains input/output ports
    and communicates with other nodes via redis subpub
    
    upon sub listen event, run functions specified in a handle_list
    
    usage:
    x = Node()
    x.add_node(...)
    ...
    x.setup_io()
    '''
    def __init__(self):
        self.ports = {}
        return
    
    def add_node(self, port_dict):
        '''
        port_dict: dict with keys: 'name', 'direction', 'ch', 'handle_list'
            name:str
            direction:str "in" or "out"
            ch: str; redis channel
            handle_list: list of tuples: [(func_1, args_1), (func_2, args_2), ...]
        '''
        self.ports[port_dict['name']] = port_dict
        return
    
    def setup_io(self, DEBUG_FLAG = False, sleep_time = 0.0005):
        r = redis.Redis()
        for key in self.ports:
            
            # TODO add redis stuff <-----------------------
            
            p = r.pubsub(ignore_subscribe_messages=True)
            channel = self.ports[key]['ch']
            # p.subscribe(channel)
            if self.ports[key]['direction'] in ['in','out']:
                # logging.info('adding input on channel: ' + channel)
                logging.info(self.ports[key]['name'] + '(' + self.ports[key]['direction'] + ') : ' + channel)
                
                # set up listener function <------------------
                # get func and args
                if self.ports[key]['handle_list']:
                    for func, kwargs in self.ports[key]['handle_list']: # TODO: remove kwargs
                        # if DEBUG_FLAG:
                            # func(**kwargs)  # evaluate all functions DEBUG
                            # threading.Thread(target=func, kwargs=kwargs).start() # evaluate all functions in a thread DEBUG
                        
                        # add handler and listen in another thread
                        p.subscribe(**{channel: func}) # note: no args here
                        thread = p.run_in_thread(sleep_time) # if sleep time is 0, causes high CPU
                
                
            # thread = p.run_in_thread()
        logging.info('done setting up i/o; subscriptions: '+str(r.pubsub_channels()))
        # print(r.pubsub_channels())
        
        self.r = r
        return
    
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    
    def func(x, y):
        import time
        time.sleep(1)
        print(x+y)
        return
    
    
    def func2(x, y):
        import time
        time.sleep(.5)
        print(x*y)
        return
    
    def func_listenerA(msg):
        print('I am A')
        print(msg)
        return
    
    def func_listenerB(msg):
        print('I am B')
        print(msg)
        return
    
    
    i1 = {'name': 'output1',
          'direction': 'out',
          'ch': 'bus',
          'handle_list': [(func_listenerA,{'msg':None})]}
    o1 = {'name': 'input1',
          'direction': 'in',
          'ch': 'wire',
          'handle_list': [(func, {'x':1.0, 'y':2.0}), (func2, {'x':2.0, 'y':4})]}
    o2 = {'name': 'input2',
          'direction': 'in',
          'ch': 'wire',
          'handle_list': [(func_listenerA,{'msg':None}), (func_listenerB,{'msg':None})]}
    
    # x = {}
    # x[i1['name']] = i1
    # print(x)
    # x[o2['name']] = o2
    # print(x)
    
    node = Node()
    node.add_node(i1)
    node.add_node(o2)
    node.setup_io(sleep_time=0.01)
    
    time.sleep(2)
    r = redis.Redis()
    _ = r.publish('wire','hi') # don't care about number of matching channel/pattern subscriptions
    _ = r.publish('bus','bye')
    # TODO: fix random BlockingIOError @ p.run_in_thread()
    # TODO: use proper listner handles; def func(msg): ...