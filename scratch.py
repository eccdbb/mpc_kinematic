from matplotlib import pyplot as plt
from functools import partial
import redis
from datetime import datetime
import os

USE_JAX = True
if USE_JAX:
    import jax.numpy as np
    import jax
else:
    import numpy as np

# class MPCOptions:
#     lb = None
#     ub = None
#     cl = None
#     cu = None
#
#     def __init__(self):
#         return


class MPCProblemStruct:
    # list here so pycharm can find
    Nx = None
    Nu = None
    N = None
    z0 = None
    lb = None
    ub = None
    cl = None
    cu = None

    def __init__(self):
        self.Nx = None
        self.Nu = None
        self.N = None
        self.z0 = None
        self.lb = None # variable lower bounds
        self.ub = None # variable upper bounds
        self.cl = None # constraint lower bounds
        self.cu = None # constraint upper bounds
        return


class MPC:
    def __init__(self, Nx:int, Nu:int, N:int):

        # general inputs
        self.Nx = None # positive real integer
        self.Nu = None # positive real integer
        self.N = None # positive real integer
        self.Ts = None # positive real number
        self.subsampling_factor = 1 # positive real integer # TODO: unused for now; therefore leave this equal to 1
        self.p = None
        self.p_scalar = None
        self.x0 = None # initial state
        self.xref = None # target state (aka xT)
        self.u0 = None # initial input

        # solver inputs
        self.ipopt_tol = 1e-5 # positive real number
        self.ipopt_max_iter = 50 # positive real integer
        self.ipopt_print_level = 5 # positive real integer
        self.auxdata = None
        self.Z0 = None # real number

        # solver outputs
        self.Z = None # solution to the MPC problem
        self.Z2 = None # reshaped solution to MPC problem, with dimensions corresponding to [problem variable, time]
        self.info = None # info from solver

        # flags
        self.debug_flag = False
        self.flag_SS_MS = 'SS' # 'SS' or 'MS'
        self.flag_use_aux = False # if true, aux constraints are appended
        self.flag_use_obj_func = True # if false, a feasibility problem (min 0 s.t. ...) is solved; if true, the external objective function is used (min f_objective s.t. ...)

        # function handles
        self.f_plant_update_mpc = None  # dynamics model x_dot = f(x,u,p,t)
        self.f_objective = None # objective function f(z)
        self.f_constraints_aux = None # additional constraints g2(z); total constraints: vertical stack: [g(z); g2(z)]
        self.f_jacobian_aux = None # d g2(z) / d z
        self.jacobianstructure_aux = None # structure of d g2(z) / d z
        self.f_bounds = None # bounds function for problem variable and equality constraints

        # private properties
        self._problem_struct = MPCProblemStruct
        #self._options = MPCOptions
        self._f_gradient = None # d f(z) / d z
        self._f_constraints = None # MPC constraints g(z)
        self._f_jacobian = None # d g(z) / d z
        self._f_jacobianstructure = None # structure of d g(z) / d z


        # construction
        print('initializing MPC instance')
        self.Nx = Nx
        self.Nu = Nu
        self.N = N
        print('constructed MPC instance with Nx={:.0f}, Nu={:.0f}, N={:.0f}'.format(self.Nx, self.Nu, self.N))

        return

    def construct(self, x0, xref, flag_SS_MS, flag_use_aux, Z0):
        '''
        construct MPC problem
        x0: initial state [Nx,1]
        xT: target state [Nx,1]
        Z0: optimization problem initialization
        '''
        self.x0 = x0
        self.xref = xref

        # initialize MPC problem variable
        # Z0 = np.zeros((self.Nu*self.N,1))
        self.Z0 = Z0

        self.flag_SS_MS = flag_SS_MS
        self.flag_use_aux = flag_use_aux
        self.flag_use_obj_func = False # by default, solve a feasibility problem; if true, must set f_objective after calling __init__()
        self.f_objective = lambda z, auxdata: 0.0

        # get bounds
        LB, UB, CLB, CUB = self.f_bounds(self.N, self.Nx, self.Nu, self.Ts, self.flag_use_aux, self.flag_SS_MS)

        # set bounds
        self._problem_struct.Nx = self.Nx
        self._problem_struct.Nu = self.Nu
        self._problem_struct.N = self.N
        self._problem_struct.z0 = self.Z0
        self._problem_struct.lb = LB
        self._problem_struct.ub = UB
        self._problem_struct.cl = CLB
        self._problem_struct.cu = CUB

        if self.debug_flag:
            print(self._problem_struct)

        return

    def IPOPT_MPC_solve(self):
        '''
        given problem inputs, solve a single MPC problem

        min f(z)
        s.t.
        lb <= z <= ub
        cl <= g(z) <= cu
        '''

        if self.flag_SS_MS=='SS':
            self._f_gradient = MPC.IPOPT_MPC_SS_objective_gradient
            self._f_constraints = MPC.IPOPT_MPC_SS_constraint
            self._f_jacobian = MPC.IPOPT_MPC_SS_constraint_jacobian
            self._f_jacobianstructure = MPC.IPOPT_MPC_SS_jacobian_structure
        elif self.flag_SS_MS=='MS':
            self._f_gradient = MPC.IPOPT_MPC_MS_objective_gradient
            self._f_constraints = MPC.IPOPT_MPC_MS_constraint
            self._f_jacobian = MPC.IPOPT_MPC_MS_constraint_jacobian
            self._f_jacobianstructure = MPC.IPOPT_MPC_MS_jacobian_structure

        z0 = self._problem_struct.z0

        self._options.lb = self._problem_struct.lb
        self._options.ub = self._problem_struct.ub
        self._options.cl = self._problem_struct.cl
        self._options.cu = self._problem_struct.cu

        self._options.ipopt.tol = self.ipopt_tol # TODO: see how to pass options to python IPOPT <================
        self._options.ipopt.max_iter = self.ipopt_max_iter
        self._options.ipopt.mu_strategy = 'adaptive'
        self._options.ipopt.hessian_approximation = 'limited-memory'
        self._options.ipopt.limited_memory_update_type = 'bfgs'
        self._options.ipopt.print_level = self.ipopt_print_level

        # self properties to auxdata
        # TODO: see how to pass auxdata for python IPOPT // @ MPC.m line 257 <=========

        # save auxdata

        # function handle properties should be set; then call IPOPT_MPC_solve() // TODO
        # TODO: in MPC.m, why this?: funcs.objective = MPC.IPOPT_MPC_SS_objective; % obj.f_objective
        funcs.objective = MPC.IPOPT_MPC_SS_objective # default
        funcs.gradient = self._f_gradient
        funcs.constraints = self._f_constraints
        funcs.jacobian = self._f_jacobian
        funcs.jacobianstructure = self._f_jacobianstructure

        # run IPOPT
        if self.debug_flag:
            print('calling IPOPT...')
        z, info2 = ipopt_auxdata(z0,funcs,options)
        self.Z = z
        self.info = info2

        if self.debug_flag:
            print(self.info)

        # postprocess
        if self.flag_SS_MS=='SS':
            self.Z2 = np.reshape(z,(-1,self.N))
        elif self.flag_SS_MS=='MS':
            z_extra = np.vstack((z,np.zeros((self.Nu,1)))) # pad with dummy 0-input, then reshape to [Nx+Nu,N+1] for convenience; remember last input in Z2 is dummy
            self.Z2 = np.reshape(z_extra, (self.Nx+self.Nu,self.N+1))


        return

    @staticmethod
    def IPOPT_MPC_SS_objective_gradient():
        return

    @staticmethod
    def IPOPT_MPC_SS_constraint():
        return

    @staticmethod
    def IPOPT_MPC_SS_constraint_jacobian():
        return

    @staticmethod
    def IPOPT_MPC_SS_jacobian_structure():
        return

    @staticmethod
    def IPOPT_MPC_MS_objective_gradient():
        return

    @staticmethod
    def IPOPT_MPC_MS_constraint():
        return

    @staticmethod
    def IPOPT_MPC_MS_constraint_jacobian():
        return

    @staticmethod
    def IPOPT_MPC_MS_jacobian_structure():
        return

    #TODO: static methods related to Environment and integration: use Environment class or copy functions to here?


class Environment:
    '''
    batch simulation environment; includes plant model and integrator

    to support batch simulation, data includes a batch dimension
    state: [N_batch,Nx]
    input: [N_batch,N,Nu]
    '''

    def __init__(self, x0, h=1e-3, integrator_type='RK4', subsampling_factor=1,
                 use_pubsub = False,
                 external_clock_channel:str = None,
                 input_channel:str = None,
                 output_channel:str = None):

        self.p = PlantParameters
        self.plant = Plant()
        self.curr_state = x0

        self.Ts = h  # sample time
        self.integrator = Integrator(plant=self.plant, type=integrator_type, h=h, subsampling_factor=subsampling_factor)

        self.flag_log = True
        self.log_state = []
        self.log_input = []
        if self.flag_log:
            self.log_state.append(self.curr_state)

        if use_pubsub:
            print('using pubsub')
            # TODO: create pubsub, listener handle, publish
            self.clock_channel = external_clock_channel
            self.output_channel = output_channel
            self.input_channel = input_channel

            # set up publish
            host = os.environ['REDIS_URL']
            port = os.environ['PORT']
            self.r = redis.Redis(host=host, port=int(port))
            self.p = self.r.pubsub(ignore_subscribe_messages=True)
            self.p.subscribe(self.output_channel)
            print('output_channel: ' + self.output_channel)

            print('clock_channel: ' + self.clock_channel)
            # listen to clock
            self.clock_listener()

            # listen to input_channel # TODO <---------------------------------------------------
            # self.input_listener()

            # listen to output_channel
            # self.output_listener() # create this

            # send first message to output_channel
            self.r.publish(self.output_channel, '!ENVIRONMENT_STARTED')
        return

    def clock_listener(self):
        ch_name = self.clock_channel
        host = os.environ['REDIS_URL']
        port = os.environ['PORT']
        r = redis.Redis(host=host, port=int(port))
        p = r.pubsub(ignore_subscribe_messages=True)
        p.subscribe(ch_name)

        # listen in another thread
        def my_handler(msg):
            if msg['type'] == 'message':
                data = msg['data']
                print(data.decode() + ' at ' + datetime.now().strftime('%Y.%m.%d-%H:%M:%S.%f'))

            # do something
            r.publish(self.output_channel,'tick received')

            # run task
            # TODO: get input signal and do Environment.step() <=============
            # example
            if msg['type'] == 'message':
                data = msg['data'].decode()
                value = data.split('=')[-1].split(' ')[-1]
                u = value
                # self.step(u)
                # publish self.log_state[-1] and self.log_input[-1]
                print('---------Ts [s]: ',self.Ts, '-------')
            return

        p.subscribe(**{ch_name: my_handler})
        thread = p.run_in_thread()
        return

    def output_listener(self):
        ch_name = self.output_channel
        host = os.environ['REDIS_URL']
        port = os.environ['PORT']
        r = redis.Redis(host=host, port=int(port))
        p = r.pubsub(ignore_subscribe_messages=True)
        p.subscribe(ch_name)

        # listen in another thread
        def my_handler(msg):
            if msg['type'] == 'message':
                data = msg['data']
                print(data.decode() + ' at ' + datetime.now().strftime('%Y.%m.%d-%H:%M:%S.%f'))

            # do other stuff; ex. run a task (MPC, update 3D pose, ...)
            # ex. data.decode() is "!VELOCITY_CMD,1.0,0.0,2*pi",
            # if data.decode().splot(',')[0] == "!VELOCITY_CMD": do something

            # run task

            return

        p.subscribe(**{ch_name: my_handler})
        thread = p.run_in_thread()
        return


    def step(self, u):# TODO: support batch simulation; state -> [N_batch,Nx], input -> [N_batch,N,Nu]; same as before, step a sequence of u <=============================
        '''
        u: input vector; can be a sequence so that u is a matrix [u[1], u[2], ..., u[N]] with shape (Nu,N)
        u.shape should be (Nu,N); ex. scalar u: (1,1); 2 inputs, 1 time step: (2,1)
        step through the entire sequence
        '''

        N = u.shape[1]
        for k in range(N):
            u_k = u[:, k]  # current input
            x = self.curr_state
            self.curr_state = self.simulator_step(x, u_k)

            if self.flag_log:
                self.log_state.append(self.curr_state) # DONE: jax.jit leak here (and next line); don't jit here
                self.log_input.append(u_k)

        return # at this point, self.curr_state has been updated


    @partial(jax.jit, static_argnums=(0,))  # https://github.com/google/jax/issues/1251
    def simulator_step(self, x, u):

        x_next = self.integrator.integrate(x, u)
        return x_next


class Integrator:
    '''collection of integrators'''

    def __init__(self, plant=None, type='RK4', h=1e-3, subsampling_factor=1):
        self.type = type  # string; RK4, euler, ...
        self.plant = plant  # class with static function call(t,x,u,p); x_dot = f(x,u,t,p,w,...); integral x_dot -> x_next
        self.h = h  # step size (seconds)
        self.subsampling_factor = subsampling_factor
        return

    def integrate(self, x, u):
        t = 0  # assume time-invariant system for now
        if self.plant is None:
            print('warning: no model set; need plant class')
            x_next = None
        else:
            if self.type == 'RK4':
                h_effective = self.h / self.subsampling_factor
                for i in range(self.subsampling_factor):
                    k_1 = self.plant.call(t, x, u)
                    k_2 = self.plant.call(t, x + 0.5 * h_effective * k_1, u)
                    k_3 = self.plant.call(t, x + 0.5 * h_effective * k_2, u)
                    k_4 = self.plant.call(t, x + h_effective * k_3, u)
                    x_next = x + (1 / 6) * (k_1 + 2 * k_2 + 2 * k_3 + k_4) * h_effective
                    x = x_next
            elif self.type == 'euler':
                print('warning: integrator subsampling not implemented for euler')
                x_next = x + self.h * self.plant.call(t, x, u)
            else:
                print('warning: integrator type not set; defaulting to euler')
                print('warning: integrator subsampling not implemented for euler')
                x_next = x + self.h * self.plant.call(t, x, u)

        return x_next


# == BEGIN PROBLEM =================================================================================
# for each unique problem, write classes: Plant, PlantParameters.
class PlantParameters:
    def __init__(self):
        self.Nx = 3  # number of states
        self.Nu = 3  # number of inputs
        self.g = 9.81  # m/s^2
        return


class Plant: # TODO: update to support batch simulation
    def __init__(self):
        self.x_dot = None
        self.p = PlantParameters()
        return

    def call(self, t, x, u):
        '''
        evaluate x_dot function

        kinematic model
        state vector: [X, Y, psi]
        input vector: [Vx, Vy, r]

        X [m] global X-position
        Y [m] global Y-position
        psi [rad.] yaw angle (heading angle)
        Vx [m/s] longitudinal velocity (ego reference frame)
        Vy [m/s] lateral velocity " "
        r [rad./s] yaw rate " "
        '''

        psi = x[2]
        Vx = u[0]
        Vy = u[1]
        r = u[2]

        x_dot = np.zeros((self.p.Nx, 1))
        # x_dot[0] = Vx*np.cos(psi) - Vy*np.sin(psi)
        # x_dot[1] = Vx*np.sin(psi) - Vy*np.cos(psi)
        # x_dot[2] = r
        x_dot = x_dot.at[0].set(Vx * np.cos(psi) - Vy * np.sin(psi)) # DONE: figure out how to set x_dot with jax
        x_dot = x_dot.at[1].set(Vx * np.sin(psi) - Vy * np.cos(psi))
        x_dot = x_dot.at[2].set(r)
        return x_dot

# == END PROBLEM =================================================================================

# class hierarchy: PlantParameters < Plant < Integrator < Environment < MPC


if __name__ == "__main__":

    print('testing: Plant')
    plant = Plant()
    print(plant.p.g)

    plant.call(0, np.zeros((3, 1)), np.zeros((3, 1)))

    print('testing Environment')
    env = Environment(x0=np.zeros((3, 1)), h=1e-1) # TODO: update x,u to support batch simulation
    N = 30  # number of time steps
    env.step(np.zeros((3, N)))
    for i in range(len(env.log_state)):
        print(env.log_state[i].flatten())

    # reset env
    env = Environment(x0=np.zeros((3, 1)), h=1e-1)
    N = 100  # number of time steps
    u = np.zeros((3, N))
    # u[0, :] = 1.0  # longitudinal velocity 1m/s
    # u[2, :] = 2 * np.pi / 10  # make a circle in 10s
    u = u.at[0, :].set(1.0)
    u = u.at[2, :].set(2 * np.pi / 10)
    u = u.at[2, -np.rint(N/2).astype(int):].set(-1 * 2 * np.pi / 10) # halfway until the end, flip input
    env.step(u)
    for i in range(len(env.log_state)):
        print(env.log_state[i].flatten())

    # plot
    x = np.array(env.log_state).squeeze().T
    fig1 = plt.figure()
    plt.subplot(3, 1, 1)
    plt.plot(x[0])
    plt.ylabel('X [m]')
    plt.subplot(3, 1, 2)
    plt.plot(x[1])
    plt.ylabel('Y [m]')
    plt.subplot(3, 1, 3)
    plt.plot(x[2])
    plt.ylabel('psi [rad.]')
    plt.xlabel('t [s])')
    fig1.savefig('fig1.png')


    fig2 = plt.figure()
    plt.plot(x[0], x[1], marker='.')
    plt.axis('equal')
    plt.xlabel('X [m]')
    plt.ylabel('Y [m]')
    fig2.savefig('fig2.png')

    fig3 = plt.figure()
    plt.subplot(311)
    plt.plot(u[0,:])
    plt.ylabel('u1')
    plt.subplot(312)
    plt.plot(u[1,:])
    plt.ylabel('u2')
    plt.subplot(313)
    plt.plot(u[2,:])
    plt.ylabel('u3')
    fig3.savefig('fig3.png')


    plt.show()
    print('-- done')

# DONE: try using JAX with Environment
# notes: use block comments to switch between numpy and jax.numpy // FAIL
# DOING: JAX jit and try comparing times; where to jit?
# TODO: from jax import jacrev, compute jacobian of Environment.step()
# TODO: jax.vmap(jax.jacrev( Environment.step() ))
# how to deal with multiple shooting? try to use Environment.simulator_step(), jax.jacrev, jax.vmap
# jax.vmap( jax.jacrev( Environment.simulator_step ), (0,0), 0 )(x_batch,u_batch) // u_batch is for a single time step; result is [N_batch,Nx,Nx] and [N_batch,Nx,Nu]
# TODO: add websockets to serve env.log_state

''' 
do this to assign value to x
quote:
None of these expressions modify the original x; instead they return a modified copy of x.
However, inside a jit() compiled function, expressions like x = x.at[idx].set(y) are guaranteed to be applied inplace.
------------------------
import jax

@jax.jit
def set(x):
    return x.at[0, :].set(1.0)

x = np.zeros((3,100))
x = set(x)

---------- or ------------
import jax.numpy as np
x = np.zeros((3,100))
x = x.at[0, :].set(1.0)

'''
