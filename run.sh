#!/bin/bash

# assuming cd mpc_kinematic
git pull
sudo docker build -f dockerfile-clock -t clock .
sudo docker build -f dockerfile-signal_gen -t signal_gen .
sudo docker-compose up