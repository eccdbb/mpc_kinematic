import threading
import time
from datetime import datetime
import redis
import logging
from clock import ClockGen
import os


class SignalGenerator:
    '''
    given an internal or external clock,
    generate a (periodic) signal
    '''
    def __init__(self, external_clock_channel:str = None, Td_internal:float = None, output_channel:str = None):
        '''
        external_clock_channel: redis channel name
        If this is used, an external clock is assumed.  Otherwise, an
        internal clock is used, and Td_internal should be set.
        
        Td_internal: period of internal clock
        
        output_channel: publish to this redis channel
        
        '''
        logging.basicConfig(level=logging.INFO)
        self.phase = 0 # unsigned integer [0,1023]
        self.output_channel = output_channel
        
        # set up publish
        host = os.environ['REDIS_URL']
        port = os.environ['PORT']
        self.r = redis.Redis(host=host, port=int(port))
        self.p = self.r.pubsub(ignore_subscribe_messages=True)
        self.p.subscribe(self.output_channel)
        
        if not external_clock_channel: # use internal clock
            self.using = 'internal'
            name = 'signal_generator_internal'
            self.clock_channel = 'clock_' + name # redis channel name
            logging.info('starting internal clock')
            # clock = ClockGen(name=self.clock_channel, Td = Td_internal) # blocking
            thread = threading.Thread(target=ClockGen, args=(name, Td_internal), daemon=True)
            thread.start()
        else: # use external clock
            self.using = 'external'
            self.clock_channel = external_clock_channel # redis channel name
            logging.info('using external clock')
            
            
        print('clock_channel: ' + self.clock_channel)
        # listen to internal or external clock
        self.clock_listener()
        
        # listen to output_channel
        self.output_listener()
        
        # send first message to output_channel
        self.r.publish(self.output_channel,'!SIGNAL_GENERATOR_STARTED')
        return
    
    def clock_listener(self):
        ch_name = self.clock_channel
        host = os.environ['REDIS_URL']
        port = os.environ['PORT']
        r = redis.Redis(host=host, port=int(port))
        p = r.pubsub(ignore_subscribe_messages=True)
        p.subscribe(ch_name)
        
        # listen in another thread
        def my_handler(msg):
            if msg['type'] == 'message':
                data = msg['data']
                print(data.decode() + ' at ' + datetime.now().strftime('%Y.%m.%d-%H:%M:%S.%f'))
    
            # do other stuff; ex. run a task (MPC, update 3D pose, ...)
            # ex. data.decode() is "!VELOCITY_CMD,1.0,0.0,2*pi",
            # if data.decode().split(',')[0] == "!VELOCITY_CMD": do something
            
            # run signal generation task
            threading.Thread(target=self.generate_signal, daemon=True).start()
            return

        p.subscribe(**{ch_name: my_handler})
        thread = p.run_in_thread()
        return

    def output_listener(self):
        ch_name = self.output_channel
        host = os.environ['REDIS_URL']
        port = os.environ['PORT']
        r = redis.Redis(host=host, port=int(port))
        p = r.pubsub(ignore_subscribe_messages=True)
        p.subscribe(ch_name)
    
        # listen in another thread
        def my_handler(msg):
            if msg['type'] == 'message':
                data = msg['data']
                print(data.decode() + ' at ' + datetime.now().strftime('%Y.%m.%d-%H:%M:%S.%f'))
        
            # do other stuff; ex. run a task (MPC, update 3D pose, ...)
            # ex. data.decode() is "!VELOCITY_CMD,1.0,0.0,2*pi",
            # if data.decode().splot(',')[0] == "!VELOCITY_CMD": do something
        
            # run task
            
            return
    
        p.subscribe(**{ch_name: my_handler})
        thread = p.run_in_thread()
        return
    
    def generate_signal(self):
        # use a DDS (phase to value LUT) to generate a signal
        val = self.DDS_step()
        
        # publish # TODO
        
        return
    
    def DDS_step(self):
        x = self.DDS_LUT()
        print('phase = ', str(self.phase), 'value = ', str(x))
        self.phase += 1
        return x
    
    def DDS_LUT(self): # implement a simple square wave
        self.phase = SignalGenerator.wrap(self.phase)
        if self.phase < 512:
            out = -1.0
        else:
            out = 1.0
        return out
    
    @staticmethod
    def wrap(x):
        if x < 0:
            logging.warning('DDS phase is negative; resetting to 0')
            x = 0
        if x > 1023:
            x = 0 # x % 1023
        return x
    

if __name__ == '__main__':
    use_external_clock = True
    if use_external_clock:
        x = SignalGenerator(external_clock_channel='clock_clkA', output_channel='signal_gen_to_plant')
    else: # use internal clock
        x = SignalGenerator(Td_internal = 1.0, output_channel='signal_gen_to_plant')
    
    # test connecting to Environment
    from scratch import Environment, Integrator, Plant, PlantParameters
    import numpy as np

    def output_listener(ch_name):
        # ch_name = self.output_channel
        host = os.environ['REDIS_URL']
        port = os.environ['PORT']
        r = redis.Redis(host=host, port=int(port))
        p = r.pubsub(ignore_subscribe_messages=True)
        p.subscribe(ch_name)

        # listen in another thread
        def my_handler(msg):
            if msg['type'] == 'message':
                data = msg['data']
                print(data.decode() + ' at ' + datetime.now().strftime('%Y.%m.%d-%H:%M:%S.%f'))

            # do other stuff; ex. run a task (MPC, update 3D pose, ...)
            # ex. data.decode() is "!VELOCITY_CMD,1.0,0.0,2*pi",
            # if data.decode().splot(',')[0] == "!VELOCITY_CMD": do something

            # run task

            return

        p.subscribe(**{ch_name: my_handler})
        thread = p.run_in_thread()
        return


    host = os.environ['REDIS_URL']
    port = os.environ['PORT']
    r = redis.Redis(host=host, port=int(port))
    p = r.pubsub(ignore_subscribe_messages=True)
    p.subscribe('env_out')
    output_listener('env_out')
    env = Environment(x0=np.zeros((3, 1)), h=0.123456789,
                      use_pubsub=True,
                      external_clock_channel='clock_clkA',
                      input_channel='signal_gen_to_plant',
                      output_channel='env_out')

    
    
# TODO: SignalGenerator -> Environment
